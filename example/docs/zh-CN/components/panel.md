::: title 基础使用
:::

::: demo

<template>
  <lay-panel><div style="padding: 30px;">面板</div></lay-panel>
</template>

<script>
import { ref } from 'vue'

export default {
  setup() {

    return {
    }
  }
}
</script>

:::

::: title 面板插槽
:::

::: table

| 插槽    | 描述     | 可选值 |
| ------- | -------- | ------ |
| default | 默认插槽 | --     |

:::
